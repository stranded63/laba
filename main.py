def joinArr(arr1, arr2):
    fullyArr = arr1 + arr2
    for i in fullyArr:
        cont = fullyArr.count(i)
        if cont > 1:
            fullyArr.remove(i)
            arr1.remove(i)
            arr2.remove(i)

    resArr = arr1 + arr2
    return resArr

count = int(input("Количество размерность/элементов для массива №1: "))
testArr1 = [int(input(f"Введите {i + 1} по счету элемент для массива №1: ")) for i in range(count)]
count = int(input("Количество размерность/элементов для массива №2: "))
testArr2 = [int(input(f"Введите {i + 1} по счету элемент для массива №2: ")) for i in range(count)]

print(joinArr(testArr1, testArr2))